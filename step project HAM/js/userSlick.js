
$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.center',
});

$('.center').slick({
    arrows: true,
    slidesToShow: 4,
    asNavFor: '.slider-for',
    centerPadding: "50px",
    centerMode: true,
    focusOnSelect: true,
    swipeToSlide: true,
    variableHeight: true
});

$('a[data-slide]').click(function (e) {
    e.preventDefault();
    const slideno = $(this).data('slide');
    $('.slider-nav').slick('slickGoTo', slideno);
});