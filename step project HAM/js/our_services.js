$(document).ready(function () {
    $('.tabs li').on('click', function () {
        let dataEvent = $(this).data('button');
        let tabsTitle = $('.tabs-title[data-button="' + dataEvent + '"]');
        let textBlockContent = $('.text-block[data-button="' + dataEvent + '"]');
        $('.tabs-title.active').removeClass('active');
        $('.text-block.active-js').removeClass('active-js');
        tabsTitle.toggleClass('active');
        textBlockContent.toggleClass('active-js');
    });

});