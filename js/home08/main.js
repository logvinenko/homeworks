const input = document.getElementById(`from`);

input.addEventListener("focus", function () {
    input.style.outline = `1px solid green`
});

function showError(container, errorMessage) {
    container.className = 'error';
    const msgElem = document.createElement('p');
    msgElem.className = "error-message";
    msgElem.innerHTML = errorMessage;
    container.appendChild(msgElem);
    input.style.outline = `1px solid red`;
    msgElem.style.color = `red`;
}
function correct(container, message) {
    container.className = `rightMessage`;
    const div = document.createElement("div");
    div.className = "message";
    container.appendChild(div);
    const msgElem = document.createElement('span');
    msgElem.innerHTML = message;
    div.appendChild(msgElem);
    const btnX = document.createElement(`button`);
    btnX.innerText = `x`;
    btnX.style.borderRadius = `20px`;
    div.appendChild(btnX);
    btnX.onclick = function () {
        let oParent = this.parentNode;
        oParent.remove();
        input.value = ``;

    };
}
function resetError(container) {
    container.className = '';
    if (container.lastChild.className === "error-message" || container.lastChild.className === "message") {
        container.removeChild(container.lastChild);
    }
}
input.addEventListener(`blur`, function () {
    validate(this.form)
});
function validate(form) {
    const elem = form.elements;
    resetError(elem.from.parentNode);
    if (!elem.from.value || elem.from.value <= 0) {
        showError(elem.from.parentNode, `Please enter correct price`);
    } else {
        correct(elem.from.parentNode, `Текущая цена:  ${result()}  `);
    }
}
function result() {

    return document.getElementById(`from`).value;
}