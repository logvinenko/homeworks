window.onload = function () {
    const btn = document.getElementById(`btnColor`);
    const themeQ = localStorage.getItem(`bgColor`);
    const bgColorWhite = `white`;
    const bgColorGrey = `grey`;
    const body =  document.getElementsByTagName(`body`)[0];
    btn.addEventListener(`click`, function () {
        const theme = localStorage.getItem(`bgColor`);

        if (theme === bgColorWhite) {
            body.style.background = bgColorGrey;
            localStorage.setItem(`bgColor`, bgColorGrey)

        } else {
            body.style.background = bgColorWhite;
            localStorage.setItem(`bgColor`, bgColorWhite)
        }

    });
    if (themeQ !== null) {
        body.style.background = localStorage.getItem(`bgColor`);
    }

};
