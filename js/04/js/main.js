





/**
 * @desc Функция которая выводит Имя и фамилию которую ввел пользователь
 * @param {string} name
 * @param {string} surName
 * @return {object}
 **/

function createNewUser() {
    let name = prompt('Введите имя:');
    let surName = prompt('Введите фамилию:');
    let newUser  = {};


    newUser.firstName = name;
    newUser.lastName = surName;


    newUser.getLogin = function () {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };
    return newUser;
}

newUser = createNewUser();
console.log(`Имя = ${newUser.firstName}`);
console.log(`Фамилия =  ${newUser.lastName}`);
console.log(`Сокращение =  ${newUser.getLogin()}`);

