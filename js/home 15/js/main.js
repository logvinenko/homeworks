$(document).ready(function () {

    $(`#menu`).on(`click`, `a`, function (event) {
        event.preventDefault();
        let id = $(this).attr(`href`),
            top = $(id).offset().top;
        $(`body, html`).animate({scrollTop: top}, 1500);
    });
    $(`#btn`).on(`click`, function () {
        const
            content = $('#design'),
            icon = $(this).find(`i`);
        icon.toggleClass(`fa-angle-double-up fa-angle-double-down`);
        content.toggle('1000');

    });

   let $btnTop = $(`.btnUp`);
        $(window).on(`scroll`, function () {
            if ($(window).scrollTop () >= 470) {
                $btnTop.fadeIn();
            }else{
                $btnTop.fadeOut();
            }
    });
    $btnTop.on(`click`, function () {
        $(`html, body`).animate({scrollTop:0}, 900)
    });
});


