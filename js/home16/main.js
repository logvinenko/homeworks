const factorialch = prompt("Введите число:");

/**
 * @return {string}
 */
function Factorial(x){
    if(x < 0){
        return prompt("Факториала отрицательного числа не существует!!!")
    }
    if(x === 0){
        return prompt("Факториал числа " + x + " = 1")
    }
    if(x > 0)
    {
        let z = 1;
        for (let i = 1; i <= x; i++)
            z *=  i;
        return (z);
    }
}

const ch = Factorial(factorialch);
alert(ch);