document.addEventListener(`DOMContentLoaded`, onReady);

function onReady() {
    document.querySelector(`.tabs`).addEventListener("click", wrightLayout);

    function wrightLayout(event) {

        if (event.target.className === 'tabs-title' && 'active') {
            const dataEvent = +event.target.dataset.tabs;
            const tabsBlock = document.getElementsByClassName(`tabs-block`);

            const tabsTittle = document.getElementsByClassName(`tabs-title`);


            for (let i = 0; i < tabsBlock.length; i++) {
                if (dataEvent === i) {
                    tabsBlock[i].style.display = `block`;
                    tabsTittle[i].classList.toggle(`active`);
                } else {
                    tabsBlock[i].style.display = `none`;
                    tabsTittle[i].classList.remove(`active`);
                }
            }


        }


    }
}
