/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    try {
        if (arguments.length === 0)
            throw new HamburgerException(`No size and stuffing`);
        if (size === stuffing)
            throw new HamburgerException(`duplicate:` + size.name);
        if (size === undefined )
            throw new HamburgerException(`invalid size`);
        if (stuffing === undefined )
            throw new HamburgerException(`No stuffing`);
        this.sizeBurger = size;
        this.stuffingBurger = stuffing;
        this.topping = [];

    } catch (error) {
        console.log(error.name + `:` + error.message)
    }

}


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {name: `SIZE_SMALL`, prise: 50, calories: 20};
Hamburger.SIZE_LARGE = {name: `SIZE_LARGE`, prise: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {name: `STUFFING_CHEESE`, prise: 10, calories: 20};
Hamburger.STUFFING_SALAD = {name: `STUFFING_SALAD`, prise: 20, calories: 5};
Hamburger.STUFFING_POTATO = {name: `STUFFING_POTATO`, prise: 15, calories: 10};
Hamburger.TOPPING_MAYO = {name: `mayo`, prise: 20, calories: 5};
Hamburger.TOPPING_SPICE = {name: `spice`, prise: 15, calories: 0};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (topping === undefined )
            throw new HamburgerException(`No topping`);
        if (!this.topping.includes(topping) ) {
            this.topping.push(topping)
        } else {
            throw new HamburgerException(`duplicate topping:`+ topping.name )
        }
    } catch (error) {
        console.log(error.name + `:` + error.message)
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if(topping === undefined)
            throw new HamburgerException(`No remove topping`);
        this.topping.splice(this.topping.findIndex(function (i) {
            if (topping.name === `spice`) {
                return i.name === `spice`
            } else {
                return topping.name === `mayo`
            }
        }), 1)
    } catch (error) {
        console.log(error.name + `:` + error.message)

    }
};
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    var array = [];
    for (   var i = 0; i < this.topping.length; i++) {
        array.push(this.topping[i].name)
    }
    return array


};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {

    return this.sizeBurger.name

};

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffingBurger.name

};

/**
 * Узнать цену гамбургера
 * @return {number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var sum = 0;

    for (var i = 0; i < this.topping.length; i++) {
        sum += this.topping[i].prise;
    }
    return sum + this.sizeBurger.prise + this.stuffingBurger.prise

};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var sum = 0;

    for (var i = 0; i < this.topping.length; i++) {
        sum += this.topping[i].calories;
    }
    return sum + this.sizeBurger.calories + this.stuffingBurger.calories
};


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException(message) {
    this.message = message;
    this.name = `HamburgerException`
}
HamburgerException.prototype = Error.prototype;
var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
// добавка из майонез
hamburger.addTopping(Hamburger.TOPPING_SPICE);

// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу


// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_SMALL); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1
