//екранування служить для правильного сприйняття рядка в js.
// Якщо потрібно в рядку з одинарними кавичка добавити апостров або взяти слово в кавички ми можимо ці символи екранувати обратним слешем ( \ )

'use strict';
function createNewUser() {
    let name = prompt('Введите имя:');
    let surName = prompt('Введите фамилию:');
    let userDate = prompt(`Ведите дату yyyy.mm.dd`);
    let nowDay = new Date();
    let date = new Date(userDate);
// debugger;
    let newUser = {};
    newUser.firstName = name;
    newUser.lastName = surName;
    newUser.birthday = userDate;

    newUser.getLogin = function () {
        return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    };
    newUser.getAge = function () {
        if (nowDay.getDay() > date.getDay() && nowDay.getMonth() <= date.getMonth() ) {
            return nowDay.getFullYear() - date.getFullYear() - 1;
        } else
            return nowDay.getFullYear() - date.getFullYear();
    };
    newUser.getPassword = function () {
        return this.firstName.charAt(0) + this.lastName.toLowerCase() + date.getFullYear();
    };
    return newUser;
}


let newUser;
newUser = createNewUser();
console.log(`Имя = ${newUser.firstName}`);
console.log(`Фамилия =  ${newUser.lastName}`);
console.log(`Сокращение =  ${newUser.getLogin()}`);
console.log(`Дата рождения =  ${newUser.birthday}`);
console.log(`Сколько лет =  ${newUser.getAge()} лет`);
console.log(`Пароль =  ${newUser.getPassword()}`);








