$(document).ready(function () {
    $(`.tabs-title`).on("click", function () {
        let tabName = $(this).data(`tabs`);
        $(`.tabs-title`).removeClass(`active`);
        $(this).addClass(`active`);
        $(`.tabs-block`).removeClass(`active`);
        const tab = $(`.tabs-block[data-tabs = "` + tabName + `"]`);
        tab.addClass(`active`);

    })
});


